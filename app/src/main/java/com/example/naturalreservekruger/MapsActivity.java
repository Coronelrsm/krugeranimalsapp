package com.example.naturalreservekruger;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.naturalreservekruger.screens.uploadphotoscreen.UploadPicFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    @BindView(R.id.add_fab)
    FloatingActionButton addFab;
    @BindView(R.id.gallery_fab)
    FloatingActionButton galleryFab;
    private GoogleMap mMap;
    boolean mLocationPermissionGranted;
    Location location = new Location(LocationManager.GPS_PROVIDER);
    private Bitmap photo;
    private byte[] image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.mapstyle));

            if (!success) {
                Log.e("MapsActivity", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivity", "Can't find style. Error: ", e);
        }

        // Add a marker in Sydney and move the camera
        LatLng kruger = new LatLng(-23.821, 31.445);
        mMap.addMarker(new MarkerOptions().position(kruger).title("Marker in Kruger"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(kruger));
        getLocationPermission();
        setZoomSettings(kruger);

    }

    private void setZoomSettings(LatLng kruger) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(kruger)
                .zoom(17)
                .bearing(90)
                .tilt(0)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void getLocationPermission() {

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            this.photo = (Bitmap) data.getExtras().get("data");

        }
    }


    @OnClick({R.id.add_fab, R.id.gallery_fab})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_fab:
                dispatchTakePictureIntent();
                convertPictureToBytes();
                navigateToUploadFragment();
                break;
            case R.id.gallery_fab:
                break;
        }
    }

    private void navigateToUploadFragment() {

        //AIXO PETA

        String picString = String.valueOf(this.photo);
        /*
        NavDirections action = MapsActivityDirections.uploadPic();
        Navigation.findNavController().navigate(action);

         */

        Bundle bundle = new Bundle();
        bundle.putString(picString, "From Activity");

        // set Fragmentclass Arguments

        UploadPicFragment fragmentPicture = new UploadPicFragment();
        fragmentPicture.setArguments(bundle);

        Intent intent = new Intent(MapsActivity.this, UploadPicFragment.class);
        startActivity(intent);

    }

    private void convertPictureToBytes() {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 90, stream);
        image = stream.toByteArray();
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("photo", image);
        startActivity(intent);
    }
}
